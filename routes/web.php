<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


Route::get('/dashboard/add-company',[CompaniesController::class,'addCompany'])->name('add-company');
Route::post('/dashboard/add-company',[CompaniesController::class,'addCompany'])->name('addCompany');

Route::get('/dashboard/json-file', [CompaniesController::class,'jsonFile'])->name('jsonFile');
Route::post('/dashboard/upload-json',[CompaniesController::class,'uploadJson'])->name('uploadJson');

Route::get('/dashboard/csv-file', [CompaniesController::class,'csvFile'])->name('csvFile');
Route::post('/dashboard/upload-csv',[CompaniesController::class,'uploadCsv'])->name('uploadCsv');
