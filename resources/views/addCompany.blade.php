<x-app-layout>
    <x-slot name="title">
        Add Company
    </x-slot>

    <div class="content">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Data</h4>
                    <p class="card-category">New Data</p>
                </div>
                <div class="card-body table-responsive">
                    <form method="post" action="{{ route('addCompany') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" id="cName" name="cName" placeholder="Company Name">
                            <br>
                            <input type="text" class="form-control" id="location" name="location" placeholder="Company Location">
                            <br>
                            <input type="text" class="form-control" id="cDescription" name="cDescription" placeholder="Company Description">
                            <br>
                            <input type="text" class="form-control" id="owner" name="owner" placeholder="Company owner">
                            <br>
                            <input type=number  class="form-control" placeholder="Company Financial Performance" min=0 step=0.01  id="financial_performance" name=financial_performance>
                            <br>
                            <input type=number class="form-control" placeholder="Company Employees Number"  id="employees_number" name=employees_number>
                            <br>
                            <input type="datetime-local" id="foundationDate" class="form-control" placeholder="Company Foundation Date"
                                   name="foundationDate" value="2000-06-12T19:30">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

</x-app-layout>
