<x-app-layout>
    <x-slot name="title">
        Dashboard
    </x-slot>
    <div class="content">
        <div class="container-fluid">
            <div class="row">

            </div>
            <div class="row">

            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-tabs card-header-primary">
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">
                                    <span class="nav-tabs-title">Tasks:</span>

                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane" id="messages">
                                </div>
                                <div class="tab-pane" id="settings">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-warning">
                            <h4 class="card-title">Data</h4>
                            <p class="card-category">New Data</p>
                        </div>
                        <div class="card-body table-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
