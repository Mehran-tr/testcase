<x-app-layout>
    <x-slot name="title">
        upload Csv File
    </x-slot>

    <div class="content">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Csv Data</h4>
                    <p class="card-category">Companies Data</p>
                </div>
                <div class="card-body table-responsive">
                    <form action="{{ route('uploadCsv') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" class="form-control" required>
                        <input type="submit" name="submit" value="Upload File" style="margin-top: 20px">

                        @if(count($errors)  > 0)
                            <div style="margin-top: 50px" class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </form>

                </div>
            </div>
        </div>

    </div>

</x-app-layout>
