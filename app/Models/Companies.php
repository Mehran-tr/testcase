<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     *@var array
     */
    protected $fillable = [
        'company_name',
        'location',
        'description',
        'owner',
        'foundation_date',
        'employees_number',
        'financial_performance',
    ];
    use HasFactory;
    public $timestamps = false;
}
